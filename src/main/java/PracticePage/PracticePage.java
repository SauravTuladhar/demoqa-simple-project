package PracticePage;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class PracticePage {
    WebDriver LoginDriver;
    @FindBy(xpath = "//input[@placeholder='First Name']")
    WebElement FirstName;

    @FindBy(xpath = "//input[@placeholder='Last Name']")
    WebElement lastName;

    @FindBy(xpath = "//input[contains(@id,'userEmail')]")
    WebElement Email;

    @FindBy(xpath = "//*[@class='custom-control-label']") ////*[@class='custom-control custom-radio custom-control-inline']/following::input[@name='gender' and @value='Female']
    List<WebElement> Gender;

    @FindBy(xpath = "//input[@id='userNumber']")
    WebElement Mobile;

    @FindBy(xpath = "//input[@id='dateOfBirthInput']")
    WebElement Dob;

    @FindBy(xpath = "//div[@role='option']")
    List<WebElement> Dobdate;

    @FindBy(xpath = "//div[@class='subjects-auto-complete__value-container subjects-auto-complete__value-container--is-multi css-1hwfws3']")
    WebElement ClickSubject;

    @FindBy(xpath = "//input[@id='subjectsInput']")
    WebElement Subject;

    @FindBy(xpath = "//*[@class='custom-control-label']")
    List<WebElement> Hobbies;

    @FindBy(xpath = "//input[@id='uploadPicture']")
    WebElement Picture;

    @FindBy(xpath = "//textarea[@id='currentAddress']")
    WebElement CurrentAdress;

    @FindBy(xpath = "//*[@id='react-select-3-input']") ////*[@id='state']
    WebElement State;

    @FindBy(xpath = "//*[@id='react-select-4-input']") ////*[text()='Select City']
    WebElement City;

    @FindBy(xpath = "//button[@type='submit']")
    WebElement LoginButton;

    @FindBy(xpath = "//div[@id='example-modal-sizes-title-lg']")
    WebElement GetFormSubmit;

    public PracticePage(WebDriver driver) {
        this.LoginDriver = driver;
        PageFactory.initElements(driver, this);
    }

    public void enterFirstName(String firstName) throws InterruptedException {
        Thread.sleep(10000);
        WebDriverWait wait = new WebDriverWait(LoginDriver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(FirstName));
        FirstName.clear();
        FirstName.sendKeys(firstName);
    }

    public void enterEmail(String lastname1) {
        WebDriverWait wait = new WebDriverWait(LoginDriver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(Email));
        Email.clear();
        Email.sendKeys(lastname1);
    }

    public void enterLastName(String lastname) {
        WebDriverWait wait = new WebDriverWait(LoginDriver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(lastName));
        lastName.clear();
        lastName.sendKeys(lastname);
    }

    public String selectGender(String gender) {

        String controlName = null;

        try {
            WebDriverWait wait = new WebDriverWait(LoginDriver, 100);

            //System.out.println("Gender is : "+Gender);

            wait.until(ExpectedConditions.visibilityOfAllElements(Gender));
            //controlName = radioDataLabel.getAttribute("data-label");
            for (WebElement content : Gender) {

                //System.out.println("Content is " +content);

                String radioOption = content.getText();
                //System.out.println("radio option:-"+radioOption);
                //System.out.println("radioValue:-"+gender);
                if (radioOption.equalsIgnoreCase(gender)) {
                    content.click();
                }
            }
        } catch (WebDriverException e) {
            controlName = null;
            //System.out.println("Control name is : "+controlName);
        }
        return controlName;
    }

    public void enterMobile(String mobile) {
        WebDriverWait wait = new WebDriverWait(LoginDriver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(Mobile));
        Mobile.clear();
        Mobile.sendKeys(mobile);
    }

    public void clickDob() {
        WebDriverWait wait = new WebDriverWait(LoginDriver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(Dob));
        Dob.clear();
        Dob.click();
    }

    public String selectDob(String dob) {
        String controlName = null;
        try {
            WebDriverWait wait = new WebDriverWait(LoginDriver, 100);

            //System.out.println("Dob date is : "+Dobdate);

            wait.until(ExpectedConditions.visibilityOfAllElements(Dobdate));
            for (WebElement content : Dobdate) {
                String text = content.getText();

                //System.out.println("Text is : "+text);
                //System.out.println("dob is : "+dob);

                if (text.contains(dob)) {
                    JavascriptExecutor executor = (JavascriptExecutor) LoginDriver;
                    executor.executeScript("arguments[0].click()", content);
                    break;
                }
            }
            controlName = Dob.getAttribute("value");

            //System.out.println("Dob name is : "+Dob);
            //System.out.println("control name is : "+controlName);

        } catch (WebDriverException e) {
            controlName = null;
        }
        return controlName;
    }

    public void enterSubject(String subject) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(LoginDriver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(Subject));
        ClickSubject.click();
        Actions actions = new Actions(LoginDriver);
        Thread.sleep(1000);
        actions.sendKeys(Subject, subject, Keys.TAB).build().perform();
        Thread.sleep(1000);
    }

    public String enterHobbies(String hobbies, String hobbies1) {
        String controlName = null;

        try {
            WebDriverWait wait = new WebDriverWait(LoginDriver, 100);

            //System.out.println("Gender is : "+Gender);

            wait.until(ExpectedConditions.visibilityOfAllElements(Hobbies));
            //controlName = radioDataLabel.getAttribute("data-label");
            for (WebElement content : Hobbies) {

                //System.out.println("Content is " +content);

                String radioOption = content.getText();
                //System.out.println("radio option:-"+radioOption);
                //System.out.println("radioValue:-"+gender);
                if (radioOption.equalsIgnoreCase(hobbies) || radioOption.equalsIgnoreCase(hobbies1)) {
                    content.click();
                }
            }
        } catch (WebDriverException e) {
            controlName = null;
            //System.out.println("Control name is : "+controlName);
        }
        return controlName;
    }

    public void uploadPicture(String picture) {
        WebDriverWait wait = new WebDriverWait(LoginDriver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(Picture));
        Picture.clear();
        Picture.sendKeys(picture);
    }

    public void enterCurrentAddress(String currentAdress) {
        WebDriverWait wait = new WebDriverWait(LoginDriver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(CurrentAdress));
        CurrentAdress.clear();
        CurrentAdress.sendKeys(currentAdress);
    }

    public void selectState(String state) {
        WebDriverWait wait = new WebDriverWait(LoginDriver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(State));
        Actions actions = new Actions(LoginDriver);
        actions.sendKeys(State, state, Keys.TAB).build().perform();
    }

    public void selectCity(String city) {
        WebDriverWait wait = new WebDriverWait(LoginDriver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(City));
        Actions actions = new Actions(LoginDriver);
        actions.sendKeys(City, city, Keys.TAB).build().perform();
    }

    public void clickLoginBtn() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(LoginDriver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(LoginButton));
        LoginButton.click();
        Thread.sleep(5000);
    }

    public String getFormSubmitMessage() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(LoginDriver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(GetFormSubmit));
        String submitMessage = GetFormSubmit.getText();
        return submitMessage;
    }
}
