package Tests;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.mail.EmailException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.xml.sax.SAXException;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.*;
import javax.mail.internet.*;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Scanner;

public class Helper {
    WebDriver driver;

    public Helper(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public static Properties prop;
    public static String dataFolderBasePath = "Data/Config/";
    public static String storedTimeStamp = timeStamp();

    public static String timeStamp() {
        DateFormat timeFormat = new SimpleDateFormat("HHmmss");
        Date date = new Date();
        String timeStamp = timeFormat.format(date);
        return timeStamp;
    }

    static {
        prop = new Properties();
    }

    //Creating instance of webDriver and returning from helper class
    private static ThreadLocal<WebDriver> webDriver = new ThreadLocal<WebDriver>();

    public static WebDriver getDriver() {
        return webDriver.get();
    }

    static void setWebDriver(WebDriver driver) {
        webDriver.set(driver);
    }

    //Method to read config XML file
    public static void readConfigFile(String configFile) {
        try {

            InputStream file = new FileInputStream(dataFolderBasePath + configFile);
            prop.loadFromXML(file);
            if (file == null) {
                System.out.println("null file");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Method to encrypt userpassword
    private static final String key = "aesEncryptionKey";

    public static String encrypt(String userId) {
        try {
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
            Cipher cipher = Cipher.getInstance("AES"); //CBC/PKCS5PADDING/AES
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            byte[] encrypted = cipher.doFinal(userId.getBytes());
            return Base64.encodeBase64String(encrypted);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    //Method to decrypt Userpassword
    public static String decrypt(String encrypted) {
        try {
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));
            return new String(original);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    //Main method to run and test methods of helper class
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the password");
        String password = scanner.nextLine();
        System.out.println("The encrypted pw is:" + encrypt(password));
        System.out.println(" The real password after decryption is: " + decrypt(encrypt(password)));
    }
}

