package Tests;

import PracticePage.PracticePage;
import org.testng.Assert;

import static Tests.Helper.*;

public class PracticeForm extends BaseTest {

    PracticePage logInObj = new PracticePage(getDriver());

    public void fillForm() {
        System.out.println("Invoking URL");
        try {
            invokeBrowser(prop.getProperty("BaseUrl"));
        } catch (Exception e) {
            System.out.println("URL not invoked");
        }

        try {
            System.out.println("Validating by URL");
            Thread.sleep(5000);
            String mainPageUrl = getDriver().getCurrentUrl();
            Assert.assertEquals(mainPageUrl, prop.getProperty("BaseUrl"));
            System.out.println("URL Validation successful");
            System.out.println("Form page open successful");
        } catch (Exception e) {
            System.out.println("Form page open failed due to" + e);
        }

        System.out.println("Filling form Started");
        try {
            System.out.println("Entering First name");
            logInObj.enterFirstName(prop.getProperty("FirstName"));

            System.out.println("Entering Last name");
            logInObj.enterLastName(prop.getProperty("lastName"));

            System.out.println("Entering Email");
            logInObj.enterEmail(decrypt(prop.getProperty("Email")));

            System.out.println("Selecting Gender");
            logInObj.selectGender(prop.getProperty("Gender"));
            System.out.println("Selecting Gender remains");

            System.out.println("Entering Mobile number");
            logInObj.enterMobile(prop.getProperty("Mobile"));

            System.out.println("Entering date Of Birth");
            logInObj.clickDob();
            logInObj.selectDob(prop.getProperty("Dob"));

            System.out.println("Entering Subject");
            logInObj.enterSubject(prop.getProperty("Subject"));

            System.out.println("Selecting Hobbies");
            logInObj.enterHobbies(prop.getProperty("Hobbies"), prop.getProperty("Hobbies1"));

            System.out.println("Uploading picture");
            logInObj.uploadPicture(prop.getProperty("Picture"));

            System.out.println("Entering Current Address");
            logInObj.enterCurrentAddress(prop.getProperty("CurrentAddress"));

            System.out.println("Entering State");
            logInObj.selectState(prop.getProperty("State"));

            System.out.println("Entering City");
            logInObj.selectCity(prop.getProperty("City"));

            System.out.println("Clicking on Submit button");
            logInObj.clickLoginBtn();

        } catch (Exception e) {
            System.out.println("Login Failed" + e);
        }
    }

    public void validateFormSubmitted() {
        try {
            System.out.println("Validating form Submitted");

            String submittedMessage = logInObj.getFormSubmitMessage();
            Assert.assertEquals(submittedMessage, prop.getProperty("FormSubmitValidationMessage"));

            System.out.println("Form submitted validation successful");

        } catch (Exception e) {
            System.out.println("Unable to validate form due to " + e);
        }
    }

    public static void invokeBrowser(String url) {
        getDriver().get(url);

    }
}
