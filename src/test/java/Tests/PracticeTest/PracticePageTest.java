package Tests.PracticeTest;

import Tests.PracticeForm;
import Tests.BaseTest;
import org.testng.annotations.Test;

public class PracticePageTest extends BaseTest {
    @Test(description = "Automation practice form Test", priority = 1)
    public void automation_practice_form() {
        PracticeForm objPracticeForm = new PracticeForm();
        objPracticeForm.fillForm();
        objPracticeForm.validateFormSubmitted();
    }
}

